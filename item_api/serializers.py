from rest_framework import serializers
from item_api.models import Item

class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = ('Item_id','Item_name')
