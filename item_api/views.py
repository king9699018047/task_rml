from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from django.http.response import HttpResponse, JsonResponse
from item_api.tasks import send_item_status_task
from .models import Item
from item_api.serializers import ItemSerializer
from rest_framework import status

# Create your views here.
#Making 2 views for GET and POST accordingly
@csrf_exempt
def Item_API(request,id=0):
    if request.method == 'POST':
        item_data = JSONParser().parse(request)
        item_serialzier = ItemSerializer(data=item_data)
        if item_serialzier.is_valid():
            item_serialzier.save()
            send_status()
            return JsonResponse("Added Successfully", status=status.HTTP_202_ACCEPTED ,safe=False)
        return JsonResponse("Failed to add the data")

    elif request.method == 'GET':
        items = Item.objects.all()
        item_serialzier=ItemSerializer(items, many=True)
        return JsonResponse(item_serialzier.data, safe=False)   

def send_status():
    send_item_status_task.delay()
    return
