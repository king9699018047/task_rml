from django.db import models

# Create your models here.
# Making a model with columns as id(pk), name and status

class Item(models.Model):
    Item_id = models.AutoField(primary_key=True)
    Item_name = models.CharField(max_length=500)
    status = models.CharField(max_length=50,default="pending")

    def __str__(self):
        return f"{self.pk} : {self.Item_name}"