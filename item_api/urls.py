from django.conf.urls import url
from django.urls.resolvers import URLPattern
from item_api import views


urlpatterns = [
    url(r'^item$',views.Item_API)
]