from __future__ import absolute_import, unicode_literals
from django.http.response import HttpResponse
from item_api.models import Item
from celery.decorators import task


# Making a task which queries and updates the latest id's status as completed
@task(name="send_item_status_task")
def send_item_status_task():
    new_item = Item.objects.latest('Item_id')
    change_status = Item.objects.filter(Item_id = new_item.Item_id).update(status= "Completed")
    return "Done"