from __future__ import absolute_import, unicode_literals

import os
from django.conf import settings
from celery import Celery
import celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'Task_RML.settings')

app = Celery('Task_RML')

# app.config_from_object('django.conf:settings', namespace='CELERY')
app.config_from_object(settings, namespace='CELERY')

app.autodiscover_tasks()

@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))