Made a Django REST API which accepts a POST method.
Used Celery as the worker and RabbitMQ as the broker.
To Run the Project
1. Install the libraries mentioned in the requirement.txt file
2. Start the broker rabbitmq server.s
3. Run the celery worker as with the command: "celery -A Task_RML.celery worker --pool=solo -l info"
4. Run the Django project with the command: "python manage.py runserver"s
